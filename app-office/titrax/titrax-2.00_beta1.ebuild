# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-misc/twin/twin-0.4.6.ebuild,v 1.4 2004/06/28 13:00:58 vapier Exp $

EAPI=8

inherit flag-o-matic

append-flags -w

MY_P=${P/_beta1/-beta}
S=${WORKDIR}/${PN/x/}-${PV/_beta1}

DESCRIPTION="TimeTracker is an Athena Widget-based program to keep track of time"
HOMEPAGE="http://www.alvestrand.no/titrax/"
SRC_URI="http://www.alvestrand.no/titrax/${MY_P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~ppc ~sparc ~beta ~hppa ~amd64 ~ia64"

DEPEND="x11-misc/imake
	x11-libs/libXaw
	dev-lang/tcl
	dev-lang/tk"

RDEPEND="dev-lang/tcl
	dev-lang/tk"

src_unpack() {
	unpack ${A}
	sed -i 's:^NONXBINDIR = .*:NONXBINDIR = /usr/bin:' ${S}/Imakefile
	sed -i 's:^PERLLIBDIR = .*:PERLLIBDIR = /usr/lib/titrax:' ${S}/Imakefile
	sed -i 's:tail +2:tail -n +2:g' ${S}/Imakefile
	sed -i 's:InstallNonExec:InstallProgram:g' ${S}/Imakefile
	sed -i 's:Alpha:Beta:g' ${S}/patchlevel.h
	sed -i 's:/local/bin/perl:/usr/bin/env perl:' ${S}/weekno.perl ${S}/titrat
	sed -i '/^extern.int.*sendto();/d' ${S}/udping.c
	cd ${S}
}

src_compile() {
	xmkmf
	emake || die
}

src_install() {
	emake install DESTDIR=${D} || die
}
